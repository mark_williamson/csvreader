# CSV Reader
A tool which implements a simple CSV Reader.

The reader reads the file but does not do anything with the result.

## Author: Mark Williamson

### Requirements

* Java 8 or better
* Gradle 5.x or better

### Building
``
gradle build uberJar
``

This builds a fat jar containing all dependencies. Only the jar file is required to run the application.

###Running
``
 java -jar build/libs/csvreader-1.0.jar  csvFile.csv 
``
