package org.ohmslaw.csvreader;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

;

public class RowTest {

    @Test
    public void testThatASimpleValidRowIsParsed() {
        List<String> headers = Arrays.asList(new String[]{"One", "Two"});
        Row row = Row.from(headers,"\"A\";\"B\"");
        row.getRowData()      ;

        assertThat(row.getRowData().entrySet().size(),equalTo(2));
        assertThat(row.getRowData().get("One"),equalTo("A"));
        assertThat(row.getRowData().get("Two"),equalTo("B"));
    }

    @Test
    public void testThatAnIntegerIsParsed() {
        List<String> headers = Arrays.asList(new String[]{"Id", "Two"});
        Row row = Row.from(headers,"12;\"B\"");
        row.getRowData()      ;

        assertThat(row.getRowData().entrySet().size(),equalTo(2));
        assertThat(row.getRowData().get("Id"),equalTo(12));
        assertThat(row.getRowData().get("Two"),equalTo("B"));
    }

    @Test
    public void testThatAFloatIsParsed() {
        List<String> headers = Arrays.asList(new String[]{"Id", "Price"});
        Row row = Row.from(headers,"12;13.45");
        row.getRowData()      ;

        assertThat(row.getRowData().entrySet().size(),equalTo(2));
        assertThat(row.getRowData().get("Id"),equalTo(12));
        assertThat(row.getRowData().get("Price"),equalTo(13.45f));
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testThatAnErrorIsThrownForMissmatchedHeadersAndColumns() {
        List<String> headers = Arrays.asList(new String[]{"One"});
        expectedEx.expect(CsvReaderException.class);
        expectedEx.expectMessage("There are missing header columns for row \"A\";\"B\"");
        Row.from(headers,"\"A\";\"B\"");

    }

    @Test
    public void testThatAnErrorIsThrownForInvalidInteger() {
        List<String> headers = Arrays.asList(new String[]{"One","Two"});
        expectedEx.expect(CsvReaderException.class);
        expectedEx.expectMessage("Not a recognised integer");
        Row.from(headers,"1;2D");

    }

    @Test
    public void testThatAnErrorIsThrownForInvalidFloat() {
        List<String> headers = Arrays.asList(new String[]{"One","Two"});
        expectedEx.expect(CsvReaderException.class);
        expectedEx.expectMessage("Not a recognised float");
        Row.from(headers,"1;2.g");

    }


}
