package org.ohmslaw.csvreader;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.net.URL;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class CsvReaderTest {

    @Test
    public void simpleReaderTest() {
        URL location = getClass().getClassLoader().getResource("data.csv");// .getProtectionDomain().getCodeSource().getLocation();

        CsvReader csvReader = CsvReader.from(location.getPath());
        Map<String, Object>[] rows = csvReader.read();
        assertThat(rows.length, equalTo(5));
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testThatAMissingFileIsHandled() {

        expectedEx.expect(CsvReaderException.class);
        expectedEx.expectMessage("Did not find file I am not there");
        CsvReader csvReader = CsvReader.from("I am not there");
        Map<String, Object>[] rows = csvReader.read();
        assertThat(rows.length, equalTo(5));
    }

}


