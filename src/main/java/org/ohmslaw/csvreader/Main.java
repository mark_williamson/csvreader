package org.ohmslaw.csvreader;


import java.util.Map;

public class Main {

    public static void main(String[] args) {
        if (args.length <1 ) {
            showUsage();
            System.exit(-1);
        }
        CsvReader csvReader = CsvReader.from(args[0]);
        Map<String,Object>[] rows =  csvReader.read();
        System.out.println("Read " + rows.length + " rows from " + args[0]);
    }





    private static void showUsage() {
        System.out.println("CSV Reader");
        System.out.println("csvReader csvFile");
    }
}
