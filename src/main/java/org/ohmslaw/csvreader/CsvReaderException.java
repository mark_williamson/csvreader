package org.ohmslaw.csvreader;




public class CsvReaderException extends RuntimeException {

    public CsvReaderException() {
    }

    public CsvReaderException(String message) {
        super(message);
    }

    public CsvReaderException(String message, Throwable cause) {
        super(message, cause);
    }
}

