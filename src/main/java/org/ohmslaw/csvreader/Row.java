package org.ohmslaw.csvreader;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Row {

    private Map<String,Object> values = new HashMap();

    public static Row from(List<String> headers, String rowData) {
        Row row = new Row();
        Pattern pattern = Pattern.compile(";");
        Scanner scanner = new Scanner(rowData);
        scanner.useDelimiter(pattern);
        int position = 0;
        while(scanner.hasNext()) {
            if (position>=headers.size()) {
                throw new CsvReaderException("There are missing header columns for row " + rowData);
            }
            row.values.put(headers.get(position),row.parseToken(scanner.next()));
            position++;
        }
        return row;
    }

    public Map<String,Object> getRowData() {
        return values;
    }

    private  Object parseToken(String token) {
        if (isAString(token)) {
            return unwrapString(token);
        } else if (isAnInt(token)) {
            try {
                return Integer.parseInt(token);
            } catch (NumberFormatException e) {
                throw new CsvReaderException("Not a recognised integer");
            }
        } else if (isAFloat(token)) {
            try {
                return Float.parseFloat(token);
            } catch (NumberFormatException e) {
                throw new CsvReaderException("Not a recognised float");
            }
        }
        throw new CsvReaderException("Not a recognised data type");
    }

    private boolean isAFloat(String token) {
        return token.contains(".");
    }

    private boolean isAnInt(String token) {
        return ! token.contains(".");
    }

    private  Object unwrapString(String token) {
        return token.substring(1,token.length()-1);
    }

    private  boolean isAString(String token) {
        return token.startsWith("\"");
    }
}
