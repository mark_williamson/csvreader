package org.ohmslaw.csvreader;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.regex.Pattern;

public class CsvReader {

    private String fileName;
    private List<String> headers = new ArrayList();
    private List<Row> rows = new ArrayList<>();

    public static CsvReader from(String fileName) {
        CsvReader reader = new CsvReader();
        reader.fileName = fileName;
        return reader;
    }
    public Map<String,Object>[] read() {
        boolean headersRead = false;
        try {
            Scanner scanner = new Scanner(new File(fileName));
            while (scanner.hasNextLine()) {
                if (!headersRead) {
                    readHeaders(scanner.nextLine());
                    headersRead = true;
                } else {
                    rows.add(Row.from(getHeaders(),scanner.nextLine()));
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            throw new CsvReaderException("Did not find file " + fileName,e);
        }
       return rows.stream().map(Row::getRowData).toArray(HashMap[]::new);
    }

    private void readHeaders(String headerRow) {
        Pattern pattern = Pattern.compile(";");
        Scanner scanner = new Scanner(headerRow);
        scanner.useDelimiter(pattern);
        while (scanner.hasNext())
            headers.add(scanner.next());
    }

    private List<String> getHeaders() {
        return headers;
    }
}
